from BMC.functions.detect_peaks import detect_peaks
import numpy as np
import math
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox


fs = 1
bLimit = 30
tLimit = 240
fileNameString = "file path"
dataType = 'Both'
topMin = 1
bottomMin = 5
guiOp = False
sample_rate = 1


def read_binary(file):
    """
    translates binary file into usable data array

    :param file: string of file name

    :return vals: array of data converted from binary (np array)
    """
    vals = np.fromfile(file, dtype='uint16')
    return vals


def read_mat(file, hr_type):
    """
    read mat file input
    :param file: input file as string
    :param hr_type: which type of data to read for
    :return: np array of data
    """
    from scipy.io import loadmat
    d = loadmat(file)
    sf = np.array(d.get('fs'))
    pp = np.array(d.get('pp'))
    ecg = np.array(d.get('ecg'))
    if hr_type is 'ECG':
        return ecg.tolist()[0]
    elif hr_type is 'PP':
        return pp.tolist()[0]
    else:
        return sf.tolist()[0][0]


def read_hdf5(file, hr_type):
    """
    read hdf5 file
    :param file: string of input file
    :param hr_type: which type of file to read for
    :return: np array of data
    """
    import h5py
    with h5py.File(file, 'r') as hf:
        sf = np.array(hf.get('fs')).item(0)
        pp = np.array(hf.get('pp'))
        ecg = np.array(hf.get('ecg'))
    if hr_type is 'ECG':
        return ecg
    elif hr_type is 'PP':
        return pp
    else:
        return sf


def get_sampling_frequency(data):
    """
    gets the first value from the data and establishes it as the sampling frequency

    :param data: the array of data read from file - contains everything
    :return fs: the sampling frequency for both ECG and PP
    """
    global fs
    fs = data[0]
    return fs


def get_ecg_data(data):
    """
    iterates through the data array to get every other value which is the ECG data

    :param data: the array of data read from file - contains everything
    :return ECG: the array of ECG data only (np array)
    """
    ecg = []
    for ECG_data in data[2::2]:
        ecg.append(ECG_data)
    return np.array(ecg)


def get_pp_data(data):
    """
    iterates through data array to get every other value which is the Pulse Plethysmograph

    :param data: the array of data read from the file - contains everything
    :return PP: the array of PP data only (np array)
    """
    pp = []
    for PP_data in data[3::2]:
        pp.append(PP_data)
    return np.array(pp)


def convert_fs_to_time(sample_freq):
    """
    converts the given sampling frequency to time elapsed per sample

    :param sample_freq: sampling frequency given from file
    :return time: amount of time elapsed per sample in seconds
    """
    time = 1/sample_freq
    return time


def rectify_nan(insig):
    """
    replaces nan values with averages of values next to it or values right before/end if at beginning/end

    :param insig: signal that needs to be rectified (np.array)
    :return insig: signal that has nans replaced
    """
    for i in range(0, len(insig)):
        if math.isnan(insig[i]):
            if i == 0:
                insig[i] = insig[1]
            elif i == len(insig) - 1:
                insig[i] = insig[i - 1]
            else:
                insig[i] = (insig[i - 1] + insig[i + 1])/2
    return insig


def peak_detection(signal, mpd=588, mph=60000):
    """
    finds the indices of the peak by using detect_peaks
    imported from github.com/demotu/BMC
    finds peaks that are different from neighbors by at least 5

    :param signal: signal to be processed (np.array)
    :param mpd: amount of distance required between peaks
    :param mph: min amount to be considered a peak
    :return indeces: the indeces at which the peaks occur (np.array)
    """
    indeces = np.array(detect_peaks(x=signal, mpd=mpd, mph=mph))
    return indeces


def get_inst_heart_rate(inst_signal):
    """
    takes the instantaneous heart rate of the array of data passed to it
    using it's size and peaks detected to generate an instantaneous heart rate
    find amount of time that would have elapsed from start to end of array
    determine the amount of peaks detected within array
    determine the ratio of peaks to time and multiply by 60 (amount of seconds in a minute) to get BPM

    :param inst_signal: selected portion of signal to use to estimate Heart Rate
    :return heartRate: beats per minute determined by from signal given
    """
    time = len(inst_signal) * convert_fs_to_time(fs)
    mpd = 10000
    mph = 65000
    while True:
        try:
            peaks = len(peak_detection(inst_signal, mpd, mph))
            try:
                heart_rate = int((peaks/time)*60)
            except ZeroDivisionError as e:
                print(e)
            if heart_rate > 500:
                mpd += 1
            if heart_rate < 0:
                mph -= 50
        except ValueError:
            print("HR detected too extreme; trying to calibrate...")
        else:
            return heart_rate

    return heart_rate


def const_update(signal, sample_rate=sample_rate):
    """
    for a given signal, processes data at a certain sample_rate to have a constant update of BPM for that rate
    finds how many data points should be looked at with sampleNum
    breaks up signal array into smaller arrays based on the sampleNum <-- size of new arrays
    sends every smaller signal array to get the instantaneous heart rate
    gets back the heart rate values for that signal

    :param signal: the signal to be converted to heartRate Values
    :param sample_rate: how many seconds should pass before updating the heartRateValue (default=3seconds)
    :return heartRateVals: the heart rate values for that given sample rate
    """
    sample_num = int(sample_rate/convert_fs_to_time(fs))
    sig_start = 0
    index_array = range(len(signal))
    iter_array = index_array[sample_num - 1::sample_num]
    heart_rate_vals = []
    for n in iter_array:
        sigs = signal[sig_start:n + 1:1]
        heart_rate_vals.append(get_inst_heart_rate(sigs))
        sig_start += sample_num
    return np.array(heart_rate_vals)


def avg_heart_rate(heart_rate_vals, time, sample_rate=sample_rate):
    """
    take the average heart rate for the given values, update according to a certain time

    :param heart_rate_vals: the calculated heart rate values in an array
    :param sample_rate: the rate at which the heart rate values were taken
    :param time: at what interval the avg_heart_rate should update (in minutes)
    :return avgHR: an array of the average rates for the time given (np.array)
    """
    while True:
        try:
            avg_index = int(time*60/sample_rate)
        except ZeroDivisionError:
            sample_rate = 1
            print('Tried to divide by 0 sample rate... setting sample rate to 1')
        else:
            avg_start = 0
            index_array = range(len(heart_rate_vals))
            iter_array = index_array[avg_index - 1::avg_index]
            avg_hr_vals = []
            for n in iter_array:
                heart_rates = heart_rate_vals[avg_start:n + 1:1]
                avg_hr_vals.append(np.average(heart_rates))
                avg_start += avg_index
            return np.array(avg_hr_vals)


def isbradycardia(heart_rate):
    """
    determines if the given heart rate suggests bradycardia by setting a lower limit of 30

    :param heart_rate: the heart rate value that needs to be screened
    :return bool: True if the heart rate suggests bradycardia and false if it doesn't
    """
    if heart_rate <= bLimit:
        return True


def istachycardia(heart_rate):
    """
    determines if the given heart rate suggests tachycardia by setting an upper limit of 240

    :param heart_rate: the heart rate value that needs to be screened
    :return bool: True if the heart rate suggests tachycardia and false if it doesn't
    """
    if heart_rate >= tLimit:
        return True


def elapsed_signal_time(index, fs_val=fs):
    """
    uses the index to calculate the elapsed signal time

    :param index: the index at which the signal is being read
        NOTE: specifically signal of ECG/PP data, not multiplexed signal
    :param fs_val: sampling frequency at which signal in was gotten
    :return timeElapsed: the amount of time elapsed from the start of reading
    """
    sample_time = convert_fs_to_time(fs_val)
    time_elapsed = index * sample_time
    return time_elapsed


def browse_open():
    """
    allows users to select a .bin file or .dat file of their choice to read in

    :return:
    """
    b_open = filedialog.askopenfilename(
        filetypes=(("binary files", "*.bin"), ("dat files", "*.dat"), ("All files", "*.*")))
    fileName.set(b_open)
    return


def read_file():
    """
    reads through binary file chosen and sets up user interface if gui chosen or asks
    for sample_rate via command line

    :return:
    """
    global sample_rate
    if guiOp:
        data = read_binary(fileName.get())
        get_sampling_frequency(data)
        display_read_file(data)
    else:
        global fs
        try:
            fs = read_hdf5(fileNameString, 'FS')
        except Exception as e:
            print('Not a hdf5 file')
        try:
            fs = read_mat(fileNameString, 'FS')
        except Exception as e:
            print('Not a mat file')
        try:
            get_sampling_frequency(read_binary(fileNameString))
        except Exception as e:
            print('cannot use read_binary on file')
        sample_rate = int(input('Please input how often should instantaneous heart rate update? (in seconds)'))
        if dataType is 'ECG':
            select_ecg(sample_rate)
        elif dataType is 'PP':
            select_pp(sample_rate)
        else:
            select_both(sample_rate)


def display_read_file(data):
    """
    displays read file daya on gui

    :param data: data from read binary file call
    :return:
    """
    global topMin
    global bottomMin
    inst_hr_title = Label(text='Instantaneous Heart Rate').place(x=30, y=90)
    sample_rate_change1 = Label(text='Updates every ').place(x=30, y=120)
    sample_rate_change2 = Label(text=' seconds').place(x=160, y=120)
    sample_rate = IntVar(myGui, value=1)
    sample_rate_entry = Entry(myGui, textvariable=sample_rate, width=2).place(x=130, y=120)
    topMin_avg_title = Label(text=' Minute Average').place(x=310, y=90)
    top_min_var = IntVar(myGui, value=topMin)
    top_min_entry = Entry(myGui, textvariable=top_min_var, width=2).place(x=280, y=90)
    bottom_min_avg_title = Label(text=' Minute Average').place(x=310, y=180)
    bottom_min_var = IntVar(myGui, value=bottomMin)
    bottom_min_entry = Entry(myGui, textvariable=bottom_min_var, width=2).place(x=280, y=180)
    elapsed_time_title = Label(text='Elapsed Time: ').place(x=30, y=270)
    brady_limit_title = Label(text='Bradycardia limit: ').place(x=60, y=400)
    global bLimit
    brady_limit = IntVar(myGui, value=bLimit)
    brady_limit_entry = Entry(myGui, textvariable=brady_limit, width=3).place(x=180, y=400)
    bLimit = brady_limit.get()
    tachy_limit_title = Label(text='Tachycardia limit: ').place(x=230, y=400)
    global tLimit
    tachy_limit = IntVar(myGui, value=tLimit)
    tachy_limit_entry = Entry(myGui, textvariable=tachy_limit, width=3).place(x=350, y=400)
    tLimit = tachy_limit.get()
    topMin = top_min_var.get()
    bottomMin = bottom_min_var.get()
    choose_signal(sample_rate.get())
    return


def choose_signal(sample_rate):
    """
    allows users to choose to display either ECG or PP values on gui

    :return:
    """
    v = IntVar()
    v.set(1)
    choose = Label(text='Select which signal data to display:').place(x=50, y=60)
    ECGchoice = Radiobutton(myGui, text='ECG', value=1, command=lambda: select_ecg(sample_rate)).place(x=290, y=60)
    PPchoice = Radiobutton(myGui, text='PP', value=2, command=lambda: select_pp(sample_rate)).place(x=345, y=60)
    Bothchoice = Radiobutton(myGui, text='Both', value=3,
                             command=lambda: select_both(sample_rate)).place(x=390, y=60)


def bradycardia_warning(index, elapsed, hr_data):
    """
    makes user aware of bradycardia through a message box

    :return:
    """
    if guiOp:
        messagebox.showwarning(title="Bradycardia Alert", message="Your heart rate is lower than normal limits")
        see10minspread(index, elapsed, hr_data)
    else:
        logging.warning("BRADYCARDIA ALERT: Your heart rate is lower than normal limits")
        graph(index, elapsed, hr_data)


def tachycardia_warning(index, elapsed, hr_data):
    """
    makes user aware of tachycardia through a message box

    :return:
    """
    if guiOp:
        messagebox.showwarning(title="Tachycardia Alert", message="Your heart rate is higher than normal limits")
        see10minspread(index, elapsed, hr_data)
    else:
        logging.warning("TACHYCARDIA ALERT: Your heart rate is higher than normal limits")
        graph(index, elapsed, hr_data)


def see10minspread(index, elapsed, hr_data):
    """
    allows user to have the option to see a 10 min history of heart rate

    :return:
    """
    see = messagebox.askyesno(title="See 10 minute history", message="Would you like to see your 10 minute history?")
    if see:
        if elapsed <= 60*10:
            display10minspread(hr_data[0:index])
        if elapsed > 60*10:
            start_time = 60*10 - elapsed
            start_index = start_time/convert_fs_to_time(fs)
            display10minspread(hr_data[start_index:index])


def display10minspread(hr_data):
    """
    displays 10 min history of heart rate on a plot

    :return:
    """
    import tkinter as tk

    import matplotlib
    matplotlib.use('TkAgg')

    from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
    import matplotlib.pyplot as plt

    root = tk.Tk()

    fig = plt.figure(1)
    plt.ion()
    t = np.arange(0.0, 3.0, 0.01)
    s = np.sin(np.pi*t)
    plt.get_cmap('jet')
    plt.plot(t, s)

    canvas = FigureCanvasTkAgg(fig, master=root)
    plot_widget = canvas.get_tk_widget()
    plot_widget.pack()
    plot_widget.draw()

    plot_widget.grid(row=0, column=0)

    '''
    import matplotlib
    matplotlib.use("TkAgg")
    from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
    from matplotlib.figure import Figure

    import tkinter as tk
    newGui = Tk()
    newGui.title("10 minute overview")
    f = Figure(figsize=(5,5), dpi=100)
    a = f.add_subplot(111)
    a.plot(np.linspace(0,10,len(hr_data)), hr_data)
    canvas = FigureCanvasTkAgg(f)
    canvas.show()
    canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)

    toolbar = NavigationToolbar2TkAgg(canvas)
    toolbar.update()
    canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
    newGui.mainloop()
    '''


def select_ecg(sample_rate):
    """
    gets the ECG signals by iterating over every other
    rectifies nan values
    gets the array of HR data for instant update
    displays instantaneous HR after every 3 seconds
    calls to update elapsed signal time if 10 seconds passed
    runs through to check if HR indicates bradycardia or tachycardia

    :param sample_rate: how many seconds before an update of HR
    :return:
    """
    if guiOp:
        ecg_data_unclean = get_ecg_data(read_binary(fileName.get()))
    else:
        try:
            ecg_data_unclean = read_hdf5(fileNameString, 'ECG')
        except Exception as e:
            print('Not an hdf5 file')
        try:
            ecg_data_unclean = read_mat(fileNameString, 'ECG')
        except Exception as e:
            print('Not a mat file')
        try:
            ecg_data_unclean = get_ecg_data(read_binary(fileNameString))
        except Exception as e:
            print('Cannot read binary')
    ecg_data = rectify_nan(ecg_data_unclean)
    ecg_hr_data = const_update(ecg_data, sample_rate)
    if guiOp:
        update_hr_gui(ecg_hr_data, sample_rate)
    else:
        update_hr(ecg_hr_data, sample_rate)
    return


def select_pp(sample_rate):
    """
    gets the PP signals by iterating over every other
    rectifies nan values
    gets the array of HR data for instant update
    displays instantaneous HR after every 3 seconds
    calls to update elapsed signal time if 10 seconds has passed
    runs through to check if HR indicates bradycardia or tachy cardia

    :param sample_rate: how many seconds before an update of HR
    :return:
    """
    if guiOp:
        pp_data_unclean = get_pp_data(read_binary(fileName.get()))
    else:
        try:
            pp_data_unclean = read_hdf5(fileNameString, 'PP')
        except Exception as e:
            print('Not a hdf5 file')
        try:
            pp_data_unclean = read_mat(fileNameString, 'PP')
        except Exception as e:
            print('Not a mat file')
        try:
            pp_data_unclean = get_pp_data(read_binary(fileNameString))
        except Exception as e:
            print('Cannot read binary')
    pp_data = rectify_nan(pp_data_unclean)
    pp_hr_data = const_update(pp_data, sample_rate)
    if guiOp:
        update_hr_gui(pp_hr_data, sample_rate)
    else:
        update_hr(pp_hr_data, sample_rate)


def select_both(sample_rate):
    """
    uses both ECG and PP data to get an average of the two and use that data to est heart rate

    :param sample_rate: how many seconds before an update of HR
    :return:
    """
    if guiOp:
        pp_data_unclean = get_pp_data(read_binary(fileName.get()))
        ecg_data_unclean = get_ecg_data(read_binary(fileName.get()))
    else:
        try:
            ecg_data_unclean = read_hdf5(fileNameString, 'ECG')
            pp_data_unclean = read_hdf5(fileNameString, 'PP')
        except Exception as e:
            print('Not a hdf5 file')
        try:
            ecg_data_unclean = read_mat(fileNameString, 'ECG')
            pp_data_unclean = read_mat(fileNameString, 'PP')
        except Exception as e:
            print('Not a mat file')
        try:
            ecg_data_unclean = get_ecg_data(read_binary(fileNameString))
            pp_data_unclean = get_pp_data(read_binary(fileNameString))
        except Exception as e:
            print('Cannot read binary')
    pp_data = rectify_nan(pp_data_unclean)
    pp_hr_data = const_update(pp_data, sample_rate)
    ecg_data = rectify_nan(ecg_data_unclean)
    ecg_hr_data = const_update(ecg_data, sample_rate)
    both_hr_data = []
    both_data = []
    for index in range(0, int((len(pp_hr_data)+len(ecg_hr_data))/2)):
        both_hr_data.append((pp_hr_data[index]+ecg_hr_data[index])/2)
    for index in range(0, int((len(pp_data)+len(ecg_data))/2)):
        both_data.append((pp_data[index]+ecg_data[index])/2)
    if guiOp:
        update_hr_gui(both_hr_data, sample_rate)
    else:
        update_hr(both_hr_data, sample_rate)


def update_hr(hr_data, sample_rate):
    """
    updates HR on GUI

    :param hr_data: Either ECG, PP, or averages of both data
    :param sample_rate: how many seconds before an update of HR
    :return:
    """
    import time
    global topMin
    global bottomMin
    top_min_input = topMin
    bottom_min_input = bottomMin
    had_bcardia = False
    had_tcardia = False
    elapsed = 0
    last_top_min_index = 0
    last_bottom_min_index = 0
    for index in range(0, len(hr_data)):
        if elapsed % sample_rate == 0:
            logging.info(
                "Instantaneous Heart Rate updating every {} seconds: {}".format(sample_rate, hr_data[index]))
        if elapsed_signal_time(index, fs) - elapsed == 10:
            logging.info("Elapsed Signal Time: {} seconds".format(elapsed_signal_time(index, fs)))
            elapsed += 10
        if elapsed % (top_min_input*60) == 0 and not elapsed == 0:
            avg_top_min = avg_heart_rate(hr_data, top_min_input, sample_rate)
            logging.info("Updated {} min average: {}".format(topMin, avg_top_min[last_top_min_index]))
            last_top_min_index += 1
        if elapsed % (bottom_min_input*60) == 0 and not elapsed == 0:
            avg_bottom_min = avg_heart_rate(hr_data, bottom_min_input, sample_rate)
            logging.info("Updated {} min average: {}".format(bottomMin, avg_bottom_min[last_bottom_min_index]))
            last_bottom_min_index += 1
        if isbradycardia(hr_data[index]) and had_bcardia:
            bradycardia_warning(index, elapsed, hr_data)
        elif isbradycardia(hr_data[index]) and not had_bcardia:
            had_bcardia = True
        elif istachycardia(hr_data[index]) and had_tcardia:
            tachycardia_warning(index, elapsed, hr_data)
        elif istachycardia(hr_data[index]) and not had_tcardia:
            had_tcardia = True
        else:
            had_bcardia = False
            had_tcardia = False
        time.sleep(1)
        elapsed += 1
    logging.info("Reached End of File for HR Vals. Rerun program with new file to continue...")
    return


def update_hr_gui(hr_data, sample_rate):
    """
    updates HR on GUI

    :param hr_data: Either ECG, PP, or averages of both data
    :param sample_rate: how many seconds before an update of HR
    :return:
    """
    global topMin
    global bottomMin
    hr_val = StringVar(None)
    heart_rate = Label(textvariable=hr_val).place(x=60, y=180)
    top_min_input = topMin
    bottom_min_input = bottomMin
    had_bcardia = False
    had_tcardia = False
    elapsed = 0
    last_top_min_index = 0
    last_bottom_min_index = 0
    for index in range(0, len(hr_data)):
        if elapsed % sample_rate == 0:
            myGui.after(sample_rate*1000, hr_val.set(str(hr_data[index])))
            myGui.update()
        if elapsed_signal_time(index, fs) - elapsed == 10:
            update_elapsed(elapsed_signal_time(index, fs))
        if elapsed % (top_min_input*60) == 0 and not elapsed == 0:
            avg_top_min = avg_heart_rate(hr_data, top_min_input, sample_rate)
            update_top_min_avg(avg_top_min[last_top_min_index])
            last_top_min_index += 1
        if elapsed % (bottom_min_input*60) == 0 and not elapsed == 0:
            avg_bottom_min = avg_heart_rate(hr_data, bottom_min_input, sample_rate)
            update_bottom_min_avg(avg_bottom_min[last_bottom_min_index])
            last_bottom_min_index += 1
        if isbradycardia(hr_data[index]) and had_bcardia:
            bradycardia_warning(index, elapsed, hr_data)
        elif isbradycardia(hr_data[index]) and not had_bcardia:
            had_bcardia = True
        elif istachycardia(hr_data[index]) and had_tcardia:
            tachycardia_warning(index, elapsed, hr_data)
        elif istachycardia(hr_data[index]) and not had_tcardia:
            had_tcardia = True
        else:
            had_bcardia = False
            had_tcardia = False
        elapsed += 1
    return


def update_elapsed(time):
    """
    update elapsed time display on GUI

    :param time: what time has passed
    :return:
    """
    elapsedSecs = Label(text=str(time) + " secs").place(x=90, y=270)


def update_top_min_avg(value):

    """
    update the first minute average value on the top of the GUI

    :param value: the avg HR at the minute
    :return:
    """
    minTopAvg = Label(text=str(value)).place(x=290, y=100)
    pass


def update_bottom_min_avg(value):
    """
    update the second minute average value on the bottom of the gui

    :param value:
    :return:
    """
    minBottomAvg = Label(text=str(value).place(x=290, y=210))
    pass


def graph(index, elapsed, hr_data):
    """
    :param index: the index on HR data where the warning was generated
    :param elapsed: how much time had elapsed before warning was generated
    :param hr_data: the data being used
    :return:
    """

    import matplotlib.pyplot as plt
    global sample_rate
    if elapsed/600 >= 1:
        back_step = 600/sample_rate
        start_index = index - back_step
        plt.plot(range(0, 600, sample_rate), hr_data[start_index:index])
        plt.ylabel("Heart Rate")
        plt.xlabel("Time (sec)")
        plt.title("Heart Rate in last Ten Minutes (or less)")
        plt.show()
    else:
        plt.plot(range(0, elapsed, sample_rate), hr_data[0:index])
        plt.ylabel("Heart Rate")
        plt.xlabel("Time (sec)")
        plt.title("Heart Rate in last Ten Minutes (or less)")
        plt.show()


def nogui():
    """
    starts the direction of reading as a non gui program
    :return:
    """
    read_file()
    return

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--gui", dest="guiO", action="store_true", default=False,
                        help='choose to use the program with the gui or command line (default: commandline)')
    parser.add_argument("file", help="The file with binary ECG/PP data you wish to read.", nargs='?',
                        const="file path", default="file path", type=str)
    parser.add_argument("bradyLimit", help="The lowest value for Heart Rate permitted before a warning shows up",
                        nargs='?', const=30, default=30, type=int)
    parser.add_argument("tachyLimit", help="The highest value for Heart Rate permitted before a warning shows up",
                        nargs='?', const=240, default=240, type=int)
    parser.add_argument("dataSelection", help="Select which Data Set to use for Heart Rate Outputs ('ECG', 'PP', or "
                                              "'Both')", nargs='?', const='Both', default='Both', type=str)
    parser.add_argument("topMinAvg", help="The minutes that should pass before updating the heart rate (option 1)",
                        nargs='?', const=1, default=1, type=int)
    parser.add_argument("bottomMinAvg", help="The minutes that should pass before updating the heart rate (option 2)",
                        nargs='?', const=5, default=5, type=int)
    args = parser.parse_args()
    fileNameString = args.file
    bLimit = args.bradyLimit
    tLimit = args.tachyLimit
    dataType = args.dataSelection
    topMin = args.topMinAvg
    bottomMin = args.bottomMinAvg
    guiOp = args.guiO

    if guiOp:
        myGui = Tk()
        myGui.geometry('450x450+200+200')
        myGui.title('Heart Rate Metrics')
        introLabel = Label(text='Welcome to Heart Rate Analyzer v1.0').pack()
        inputLabel = Label(text='Input file:').place(x=15, y=35)
        fileName = StringVar(myGui, value=fileNameString)
        fileEntry = Entry(myGui, textvariable=fileName).place(x=85, y=30)
        browse = Button(myGui, text='Browse', command=browse_open).place(x=280, y=30)
        useFile = Button(myGui, text='Use File', command=read_file).place(x=355, y=30)
        myGui.mainloop()

    else:
        import logging
        logger = logging.getLogger('example')
        logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] %(asctime)s %(message)s',
                            datefmt='%m/%d/%Y %I:%M:%S %p')
        logging.info('Heart Rate Monitor initiated')
        '''
        import os
        script_path = os.path.abspath(__file__) # i.e. /path/to/dir/foobar.py
        script_dir = os.path.split(script_path)[0] #i.e. /path/to/dir/
        rel_path = fileNameString
        abs_file_path = os.path.join(script_dir, rel_path)
        fileNameString = abs_file_path
        '''
        nogui()
