import numpy as np


def test_read_hdf5():
    ecg = [60, 50, 40, 30, 10]
    fs = 1
    pp = [60, 50, 40, 30, 10]
    import h5py
    with h5py.File('test.h5', 'w') as f:
        f.create_dataset('ECG', data=ecg)
        f.create_dataset('PP', data=pp)
        f.create_dataset('FS', data=fs)
    from heartRate import read_hdf5
    assert np.array_equal(read_hdf5('test.h5', 'ECG'), [60, 50, 40, 30, 10])
    assert np.array_equal(read_hdf5('test.h5', 'PP'), [60, 50, 40, 30, 10])
    assert read_hdf5('test.h5', 'FS') is 1


def test_read_mat():
    ecg = [60, 50, 40, 30, 10]
    fs = 1
    pp = [60, 50, 40, 30, 10]
    import scipy.io as sio
    sio.savemat('test.mat', {'FS': fs, 'ECG': ecg, 'PP': pp})
    from heartRate import read_mat
    assert np.array_equal(read_mat('test.mat', 'ECG'), [60, 50, 40, 30, 10])
    assert np.array_equal(read_mat('test.mat', 'PP'), [60, 50, 40, 30, 10])
    assert read_mat('test.mat', 'FS') is 1


def test_read_binary():
    test = "test01.bin"
    from heartRate import read_binary
    assert np.array_equal(read_binary(test)[0:3], [20000, 32768, 22077])


def test_get_ecg_data():
    from heartRate import read_binary
    from heartRate import get_ecg_data
    assert np.array_equal(get_ecg_data(read_binary('test01.bin')[0:3]), [22077])
    assert np.array_equal(get_ecg_data([0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1]), [1, 1, 1, 1, 1, 1])


def test_get_pp_data():
    from heartRate import read_binary
    from heartRate import get_pp_data
    assert np.array_equal(get_pp_data(read_binary('test01.bin')[0:4]), [32783])
    assert np.array_equal(get_pp_data([0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1]), [1, 1, 1, 1, 1, 1])


def test_convert_fs_to_time():
    from heartRate import convert_fs_to_time
    assert convert_fs_to_time(0.25) == 4
    assert convert_fs_to_time(4) == 0.25


def test_rectify_nan():
    from heartRate import rectify_nan
    insig = np.array([150, 200, np.nan, 300])
    rect = rectify_nan(insig)
    assert np.array_equal(rect, [150, 200, 250, 300])
    insig = np.array([np.nan, 200, 250, 300])
    rect = rectify_nan(insig)
    assert np.array_equal(rect, [200, 200, 250, 300])
    insig = np.array([150, 200, 250, np.nan])
    rect = rectify_nan(insig)
    assert np.array_equal(rect, [150, 200, 250, 250])


def test_peak_detection():
    from heartRate import peak_detection
    insig = np.array([150, 200000, 350, 300, 200, 150])
    peak = peak_detection(insig)
    assert np.array_equal(peak, [1])
    insig = np.array([150, 200, 350, 350, 400000, 300, 200, 150])
    peak = peak_detection(insig)
    assert np.array_equal(peak, [4])
    insig = np.array([0, 1, 0, 1, 2, 1, 4, 4, 200000, 4, 4, 2, 2, 200000, 1])
    peak = peak_detection(insig, mpd=2)
    assert np.array_equal(peak, [8, 13])


def test_get_inst_heart_rate():
    from heartRate import get_inst_heart_rate
    insig = np.array([5, 20, 800, 5, 3000000, 20])
    assert get_inst_heart_rate(insig) == 10


def test_const_update():
    from heartRate import const_update
    insig = np.array([0, 50, 2, 4, 50000000, 3, 0, 2, 0, 2, 5000, 20])
    sample_rate = 3
    assert np.array_equal(const_update(insig, sample_rate), [0, 20, 0, 0])
    insig = np.array([0, 50, 2, 4, 50, 3, 0, 2, 0, 2, 50000000, 20])
    assert np.array_equal(const_update(insig, sample_rate), [0, 0, 0, 20])


def test_avg_heart_rate():
    from heartRate import avg_heart_rate
    inval = np.array([20, 20, 20, 20, 30, 20])
    assert np.array_equal(avg_heart_rate(inval, 1, 30), [20, 20, 25])


def test_isbradycardia():
    from heartRate import isbradycardia
    assert not isbradycardia(80)
    assert isbradycardia(20)


def test_istachycardia():
    from heartRate import istachycardia
    assert not istachycardia(200)
    assert istachycardia(300)


def test_elapsed_time():
    from heartRate import elapsed_signal_time
    assert elapsed_signal_time(3) == 3
    assert elapsed_signal_time(2, 2) == 1
    assert elapsed_signal_time(10, 0.5) == 20
