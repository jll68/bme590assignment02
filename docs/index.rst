.. HeartRateMonitor documentation master file, created by
   sphinx-quickstart on Fri Oct 14 01:55:10 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HeartRateMonitor's documentation!
============================================

.. toctree::
   :maxdepth: 2

   heartRate
   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
