Heart Rate Analyzer v1.0 README
*******************************
USER INSTRUCTIONS
=================
program implements a module for peak detection from github.com/demotu/BMC
to run program must do the following commands:
git submodule init
git submodule update

program uses tkinter GUI which should open upon running of program
input file must be .dat or .bin
